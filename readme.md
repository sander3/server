Debian 8.2.0 installation steps:

1.	Install
2.	English - English
3.	other
4.	Europe
5.	Netherlands
6.	United States - en_US.UTF-8
7.	American English
8.	{{ hostname }}
9.	{{ domain name }}
10.	{{ root password }}
11.	{{ full name }}
12.	{{ username }}
13. {{ password }}
14.	Guided - use entire disk
15.	Seperate /home, /var, and /temp partitions
16.	Finish partitioning and write changes to disk
17. Netherlands
18.	ftp.nl.debian.org
19.	Software to install:
	- SSH server
	- standard system utilities
20.	Login as root user.
21.	apt-get install sudo
22.	usermod -a -G sudo {{ username }}
23.	Logout.
24.	Done.