#!/bin/bash

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
source ~/.bashrc
nvm install node
npm install -g grunt-cli gulp bower pm2
if [ ! -a ~/.inputrc ]; then
	echo "\$include /etc/inputrc" > ~/.inputrc
fi
echo "set completion-ignore-case on" >> ~/.inputrc
mkdir -p ~/sites/default
tee ~/sites/default/index.php > /dev/null <<EOT
<?php

echo "You aren't supposed to B. here.";

EOT