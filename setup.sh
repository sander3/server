#!/bin/bash

sudo apt-get clean
sudo echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list
sudo apt-get update
sudo apt-get upgrade -y || sudo apt-get dist-upgrade -y

sudo apt-get install apache2 apache2-mpm-event mysql-server redis-server php5 php5-fpm php5-mysql phpmyadmin build-essential libssl-dev git -y
sudo apt-get install python-certbot-apache -t jessie-backports
sudo dpkg-reconfigure -plow unattended-upgrades
wget -qO- https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo a2enmod proxy_fcgi headers rewrite

git clone https://bitbucket.org/sander3/server.git
sudo cp -R --no-preserve=mode,ownership ~/server/etc/. /etc/

sudo a2enconf php5-fpm
sudo mysql_secure_installation
sudo adduser deploy
sudo usermod -a -G www-data deploy

sudo service php5-fpm restart
sudo service apache2 restart

sudo echo "0 */12 * * * /usr/bin/certbot renew --quiet" | crontab -

sudo apt-get autoremove -y
sudo apt-get autoclean